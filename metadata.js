import l10n from '../scripts/l10n/proxy.js'

export default {
  name: l10n.writing_app_name,
  description: l10n.writing_app_description
}
