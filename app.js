import metadata from './metadata.js'
import { $ } from '/scripts/dom.js'
import l10n from '/scripts/l10n/proxy.js'
import '/scripts/bubles.js'

$('h1').innerText = metadata.name
$('p').innerText = metadata.description
